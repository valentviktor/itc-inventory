<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('pages.category.index', [
            'title' => 'Category',
            'categories' => Category::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.category.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required|unique:categories|max:255',
            ], [
                'name.required' => 'Nama tidak boleh kosong!',
                'name.unique' => 'Nama telah digunakan!'
            ]);
    
            Category::create([
                'name' => $request->name
            ]);
    
            return back()->with('success', 'Berhasil menambahkan category!');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category)
    {
        return view('pages.category.edit', [
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required|unique:categories,id|max:255',
        ], [
            'name.required' => 'Nama tidak boleh kosong!',
            'name.unique' => 'Nama telah digunakan!'
        ]);

        $category->update([
            'name' => $request->name
        ]);

        return back()->with('success', 'Berhasil mengubah category!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return back()->with('success', 'Berhasil menghapus category '. $category->name);
    }
}
