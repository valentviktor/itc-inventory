<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (request()->ajax()) {
            $data = Product::query();
            return DataTables::of($data)
                ->order(function ($row) use ($data) {
                    if (request()->has('order') && !empty(request()->has('order'))) {
                        $order = request()->get('order')[0];
                        $column = match ((int) $order['column']) {
                            0 => 'created_at',
                            default => 'created_at'
                        };
                        $data->orderBy($column, $order['dir']);
                    } else {
                        $data->orderBy('created_at', 'desc');
                    }
                })
                ->addColumn('id', fn ($e) => $e->id)
                ->addColumn('gambar', fn ($e) => '<a href="'.asset(Storage::url($e->image)).'" data-fancybox="product" data-caption="'. $e->name .'" title="Click to zoom"><img src="'.asset(Storage::url($e->image)).'" width="150" /></a>')
                ->addColumn('nama', fn ($e) => $e->name)
                ->addColumn('kategori', fn ($e) => $e->category->name)
                ->addColumn('category_id', fn ($e) => $e->category_id)
                ->addColumn('stock', fn ($e) => $e->stock)
                ->addColumn('action', function ($e) {
                    return '<button class="btn btn-sm btn-warning edit" data-url="'.route('product.update', $e).'">Edit</button><button class="btn btn-sm btn-danger ml-1 delete" data-url="'.route('product.destroy', $e).'">Hapus</button>';
                })
                ->rawColumns(['gambar', 'action'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('pages.product.index', [
            'title' => 'Product',
            'categories' => Category::select('id', 'name')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => ['required'],
            'name' => ['required'],
            'stock' => ['required', 'numeric'],
            'description' => ['nullable'],
            'image' => ['required', 'file']
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $path = null;

        try {
            DB::beginTransaction();
            
            if ($request->hasFile('image')) {
                $path = $request->file('image')->store('public/product-image');
            }

            // Category::find($request->category_id)->products()->create([
            //     'name' => $request->name,
            //     'stock' => $request->stock,
            //     'description' => $request->description,
            //     'image' => $path,
            // ]);

            Product::create([
                'category_id' => $request->category_id,
                'name' => $request->name,
                'stock' => $request->stock,
                'description' => $request->description,
                'image' => $path,
                'first_input_by' => Auth()->user()->id,
                'last_edited_by' => Auth()->user()->id,
            ]);

            DB::commit();

            return response()->json([
                'message' => 'Product berhasil di simpan.'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            // Jika proses gagal hapus gambarnya
            if (Storage::exists($path)) {
                Storage::delete($path);
            }

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => ['required'],
            'name' => ['required'],
            'stock' => ['required', 'numeric'],
            'description' => ['nullable'],
            'image' => ['nullable']
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $path = null;

        try {
            DB::beginTransaction();
            
            $product = Product::find($id);

            $old_path = $product->image;

            if ($request->hasFile('image')) {
                $path = $request->file('image')->store('public/product-image');
            }

            $product->update([
                'category_id' => $request->category_id,
                'name' => $request->name,
                'stock' => $request->stock,
                'description' => $request->description,
                'image' => $path ?? $old_path,
                'last_edited_by' => Auth()->user()->id,
            ]);

            DB::commit();

            // Hapus gambar lama, setelah proses berhasil
            if (Storage::exists($old_path) && !is_null($path)) {
                Storage::delete($old_path);
            }

            return response()->json([
                'message' => 'Product berhasil di update.'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            // Jika proses gagal hapus gambarnya
            if (Storage::exists($path)) {
                Storage::delete($path);
            }

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            DB::beginTransaction();
            
            $product = Product::find($id);

            $old_path = $product->image;

            $product->delete();

            DB::commit();

            if (Storage::exists($old_path)) {
                Storage::delete($old_path);
            }

            return response()->json([
                'message' => 'Product berhasil di hapus.'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
