@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Product</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Product v1</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-12">
                    <button class="btn btn-success mb-3" data-toggle="modal" data-target="#modal-tambah">+ Tambah</button>
                    @if (session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-check"></i> Success!</h5>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="table" class="table table-hover text-nowrap" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Gambar</th>
                                        <th>Nama</th>
                                        <th>Kategori</th>
                                        <th>Stock</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <div class="modal fade" id="modal-tambah">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Product</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('category.store') }}" method="POST">
                        <div class="error-message"></div>
                        @csrf
                        <div class="form-group">
                            <label>Pilih Kategori</label>
                            <select class="form-control" name="category_id">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Product</label>
                            <input type="text" class="form-control" name="name" placeholder="Nama Product">
                        </div>
                        <div class="form-group">
                            <label>Stok Awal</label>
                            <input type="number" class="form-control" name="stock" placeholder="Stok">
                        </div>
                        <div class="form-group">
                            <label>Gambar Product</label>
                            <input type="file" id="image" class="form-control" name="image">
                        </div>
                        <div class="form-group">
                            <label>Deskripsi Product</label>
                            <textarea class="form-control" name="description" placeholder="Deskripsi Product"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary save">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Product</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('category.store') }}" method="PUT">
                        <div class="error-message"></div>
                        @csrf
                        <div class="form-group">
                            <label>Pilih Kategori</label>
                            <select class="form-control" name="category_id">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Product</label>
                            <input type="text" class="form-control" name="name" placeholder="Nama Product">
                        </div>
                        <div class="form-group">
                            <label>Stok Awal</label>
                            <input type="number" class="form-control" name="stock" placeholder="Stok">
                        </div>
                        <div class="form-group">
                            <label>Gambar Product</label>
                            <input type="file" id="image" class="form-control" name="image">
                            <div class="edit-image-preview"></div>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi Product</label>
                            <textarea class="form-control" name="description" placeholder="Deskripsi Product"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary update">Update</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('css')
    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/2.0.8/css/dataTables.bootstrap4.min.css"> --}}
    <link rel="stylesheet" href="{{ asset('') }}plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('') }}plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('') }}plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
@endpush

@push('js')
    {{-- <script src="https://cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/2.0.8/js/dataTables.bootstrap4.min.js"></script> --}}
    <script src="{{ asset('') }}plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('') }}plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('') }}plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

    <script>
        $(function() {
            Fancybox.bind('[data-fancybox="product"]', {});

            let table = $('#table').DataTable({
                processing: true,
                serverSide: true,
                searching: true,
                ajax: {
                    url: "{{ url()->current() }}",
                },
                columns: [{
                        data: 'id',
                        orderable: true,
                        className: ''
                    },
                    {
                        data: 'gambar',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nama',
                        orderable: true,
                        className: 'text-center'
                    },
                    {
                        data: 'kategori',
                        orderable: true,
                        className: 'text-center'
                    },
                    {
                        data: 'stock',
                        orderable: true,
                        className: 'text-center'
                    },
                    {
                        data: 'action',
                        orderable: true,
                        className: 'text-center'
                    },
                ],
                order: [
                    [0, "desc"]
                ],
                scrollX: true,
                responsive: true,
            });

            $(document).on('click', '.save', function(e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": `{{ csrf_token() }}`,
                    },
                });

                var files = $('#modal-tambah form #image')[0].files;

                let formData = new FormData();
                formData.append('image', files[0]);
                formData.append('category_id', $('#modal-tambah form [name="category_id"]').val());
                formData.append('name', $('#modal-tambah form [name="name"]').val());
                formData.append('stock', $('#modal-tambah form [name="stock"]').val());
                formData.append('description', $('#modal-tambah form [name="description"]').val());

                $.ajax({
                    type: "POST",
                    url: `{{ route('product.store') }}`,
                    async: true,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    timeout: 60000,
                    success: function(response) {
                        table.draw();
                        $('#modal-tambah form')[0].reset();
                        $('#modal-tambah form').trigger('reset');
                        $('#modal-tambah').modal('hide');
                        Swal.fire({
                            title: "Berhasil!",
                            text: response.message,
                            icon: "success"
                        });
                    },
                    error: function(error) {
                        if (error.status === 400) {
                            const errors = error.responseJSON.errors;
                            let validation = [];
                            for (const error in errors) {
                                validation.push(`<div>- ${errors[error][0]}</div>`);
                            }
                            $('#modal-tambah form .error-message').html(
                                `<div class="alert alert-danger" role="alert">${validation.join('')}</div>`
                                );
                        }
                    }
                });
            });

            $(document).on('click', '.edit', function(e) {
                e.preventDefault();
                const route = $(this).data('url');
                const data = table.row($(this).closest('tr')).data();
                for (const e in data) {
                    console.log(e);
                    if (e == 'gambar' || e == 'image') {
                        $('.edit-image-preview').html(data[e]);
                        continue;
                    }
                    $(`#modal-edit form [name=${e}]`).val(data[e]).trigger('change');
                    $(`#modal-edit form`).attr('action', route);
                }
                $('#modal-edit').modal('show');
            });

            $(document).on('click', '.update', function(e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        "X-CSRF-TOKEN": `{{ csrf_token() }}`,
                    },
                });

                var files = $('#modal-edit form input#image')[0].files;

                let formData = new FormData();
                formData.append('image', files[0]);
                formData.append('category_id', $('#modal-edit form select[name="category_id"]').val());
                formData.append('name', $('#modal-edit form input[name="name"]').val());
                formData.append('stock', $('#modal-edit form input[name="stock"]').val());
                formData.append('description', $('#modal-edit form textarea[name="description"]').val());
                formData.append('_method', 'PUT'); // Agar tetap menggunakan method put

                $.ajax({
                    type: "POST", // tetap set menjadi post agar bisa mengirimkan file
                    url: $('#modal-edit form').prop('action'),
                    async: true,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    timeout: 60000,
                    success: function(response) {
                        table.draw();
                        $('#modal-edit form')[0].reset();
                        $('#modal-edit form').trigger('reset');
                        $('#modal-edit').modal('hide');
                        Swal.fire({
                            title: "Berhasil!",
                            text: response.message,
                            icon: "success"
                        });
                    },
                    error: function(error) {
                        if (error.status === 400) {
                            const errors = error.responseJSON.errors;
                            let validation = [];
                            for (const error in errors) {
                                validation.push(`<div>- ${errors[error][0]}</div>`);
                            }
                            $('.error-message').html(
                                `<div class="alert alert-danger" role="alert">${validation.join('')}</div>`
                                );
                        }
                    }
                });
            });

            $(document).on('click', '.delete', function(e) {
                e.preventDefault();
                Swal.fire({
                    title: "Are you sure?",
                    text: "Data akan dihapus permanen dari sistem!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Yes, delete it!"
                }).then((result) => {
                    if (result.isConfirmed) {
                      const route = $(this).data("url");

                      $.ajaxSetup({
                          headers: {
                              "X-CSRF-TOKEN": `{{ csrf_token() }}`,
                          },
                      });

                      $.ajax({
                          type: "DELETE", // 
                          url: route,
                          success: function(response) {
                              table.draw();
                              Swal.fire({
                                  title: "Berhasil!",
                                  text: response.message,
                                  icon: "success"
                              });
                          },
                          error: function(error) {
                              Swal.fire({
                                  title: "Berhasil!",
                                  text: "Terjadi Kesalahan",
                                  icon: "success"
                              });
                          }
                      });
                    }
                });
            });
        });
    </script>
@endpush
